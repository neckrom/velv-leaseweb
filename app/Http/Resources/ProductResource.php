<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'location_id'   => $this->location_id,
            'location_name' => $this->location->name,
            'ram_size'      => $this->ram_size,
            'ram_type'      => $this->ram_type,
            'hdd_quantity'  => $this->hdd_quantity,
            'hdd_size'      => $this->hdd_size,
            'hdd_type_id'   => $this->hdd_type_id,
            'hdd_type_name' => $this->hddType->name,
            'hdd_capacity'  => $this->hdd_capacity,
            'price'         => $this->price,
        ];
    }
}
