<?php

namespace App\Imports;

use App\Helpers\GeneralHelpers;
use App\Models\HddType;
use App\Models\Location;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ImportProduct implements ToModel, WithStartRow, ImportExcelInterface
{
    protected static string $regex_capture_ram = '/([0-9]+)([A-Z]B)([A-Z0-9]+)/i';

    protected static string $regex_capture_hdd = '/([0-9]+)x([0-9]+)([A-Z]B)([A-Z0-9]+)/i';

    protected static int $col_name = 0;

    protected static int $col_ram = 1;

    protected static int $col_hdd = 2;

    protected static int $col_location = 3;

    protected static int $col_price = 4;

    protected $name;

    protected $ram_size;

    protected $location_id;

    protected $ram_type;

    protected $hdd_quantity;

    protected $hdd_size;

    protected $hdd_type_id;

    protected $price;

    protected $ram_decoded;

    protected $hdd_decoded;

    protected $row;

    public function model(array $row)
    {
        $this->row = $row;

        $this->extractData();

        return $this->getModel();
    }

    public function startRow(): int
    {
        return 2;
    }

    public function getModel(): Model
    {
        return new Product([
            'name'         => $this->name,
            'location_id'  => $this->location_id,
            'ram_size'     => $this->ram_size,
            'ram_type'     => $this->ram_type,
            'hdd_quantity' => $this->hdd_quantity,
            'hdd_size'     => $this->hdd_size,
            'hdd_type_id'  => $this->hdd_type_id,
            'price'        => $this->price,
        ]);
    }

    public function extractData(): void
    {
        $this->ram_decoded = self::decodeWithPattern($this->row[self::$col_ram], self::$regex_capture_ram);
        $this->hdd_decoded = self::decodeWithPattern($this->row[self::$col_hdd], self::$regex_capture_hdd);

        $this->name         = $this->row[self::$col_name];
        $this->location_id  = Location::findByExcelValue($this->row[self::$col_location])->id ?? null;
        $this->ram_size     = $this->ram_decoded[1] ?? null;
        $this->ram_type     = $this->ram_decoded[3] ?? null;
        $this->hdd_quantity = $this->hdd_decoded[1] ?? null;
        $this->hdd_size     = GeneralHelpers::convertToGB($this->hdd_decoded[2] . $this->hdd_decoded[3]);
        $this->hdd_type_id  = HddType::where('name', $this->hdd_decoded[4])->get()->first()->id ?? null;
        $this->price        = preg_replace('/[^0-9]/', '', $this->row[self::$col_price]) / 100;
    }

    protected static function decodeWithPattern(string $value, string $pattern): array
    {
        $matches_array = [];
        preg_match($pattern, $value, $matches_array);

        return $matches_array;
    }
}
