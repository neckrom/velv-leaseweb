<?php

namespace App\Imports;

use Illuminate\Database\Eloquent\Model;

interface ImportExcelInterface
{
    /**
     * @return int
     */
    public function extractData(): void;
    public function getModel(): Model;
}
