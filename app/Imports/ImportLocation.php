<?php

namespace App\Imports;

use App\Models\Location;

class ImportLocation extends ImportProduct
{
    protected static int $location_code_length = 6;

    public function getModel(): Location
    {
        $location_value = $this->row[self::$col_location] ?? null;

        $location = Location::findByExcelValue($location_value);

        if ($location) {
            return $location;
        }

        return new Location([
            'name' => substr($location_value, 0, self::$location_code_length * -1),
            'code' => substr($location_value, self::$location_code_length * -1)
        ]);
    }
}
