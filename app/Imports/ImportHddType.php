<?php

namespace App\Imports;

use App\Models\HddType;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ImportHddType extends ImportProduct
{
    public function getModel(): HddType
    {
        $hdd_name = $this->hdd_decoded[4];
        $hdd_type = HddType::where('name', $hdd_name)->get()->first();

        // if exists return
        if ($hdd_type) {
            return $hdd_type;
        }

        // else create a new one
        return new HddType([
            'name' => $hdd_name,
        ]);
    }
}

//
//class ImportHddType implements ToModel, WithStartRow
//{
//    /**
//     * @param array $row
//     *
//     * @return \Illuminate\Database\Eloquent\Model|null
//     */
//    protected static int $excel_row_column = 2;
//
//    protected static string $regex_capture = '/([0-9]+)x([0-9]+)([A-Z]B)([A-Z0-9]+)/i';
//
//    /**
//     * @var int Match group of regex pattern position 0 is always the original string
//     */
//    protected static int $regex_capture_group_index = 4;
//
//    public function model(array $row)
//    {
//        $hdd_type_array = [];
//        $this_value     = $row[self::$excel_row_column];
//
//        preg_match(self::$regex_capture, $this_value, $hdd_type_array, );
//
//        $hdd_value = $hdd_type_array[self::$regex_capture_group_index] ?? null;
//
//        if (!$hdd_value) {
//            return null;
//        }
//
//        $hdd_type = HddType::where('name', $hdd_value)->get()->first();
//
//        if ($hdd_type) {
//            return $hdd_type;
//        }
//
//        return new HddType([
//            'name' => $hdd_value
//        ]);
//    }
//
//    public function startRow(): int
//    {
//        return 2;
//    }
//}
