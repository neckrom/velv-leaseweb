<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class HddType extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use Attachable;
    use SoftDeletes;

    protected $fillable = [
        'name',
    ];

    protected $allowedSorts = [
        'name'
    ];

    protected $allowedFilters = [
        'name'
    ];

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
}
