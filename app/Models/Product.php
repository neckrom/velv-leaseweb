<?php

namespace App\Models;

use App\Helpers\GeneralHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Product extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use Attachable;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'location_id',
        'ram_size',
        'ram_type',
        'hdd_quantity',
        'hdd_size',
        'hdd_type_id',
        'price',
        'hdd_capacity',
    ];

    protected $allowedFilters = [
        'name',
        'location_id',
        'ram_size',
        'ram_type',
        'hdd_quantity',
        'hdd_size',
        'hdd_type_id',
        'price',
        'hdd_capacity',
    ];

    protected $allowedSorts = [
        'id',
        'name',
        'location_id',
        'ram_size',
        'ram_type',
        'hdd_quantity',
        'hdd_size',
        'hdd_type_id',
        'price',
        'hdd_capacity',
    ];

    protected $with = ['location', 'hddType'];

    /**
     * Ensure the price is saved as integer (for rounding issues).
     *
     * @param $value
     */
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = $value * 100;
    }

    /**
     * Ensure the price is converted back to float for presentation.
     *
     * @param $value
     *
     * @return float|int
     */
    public function getPriceAttribute($value)
    {
        return $value / 100;
    }

    /**
     * Ensure data is saved in GB value.
     *
     * @param $value
     */
    public function setHddSizeAttribute($value)
    {
        $this->attributes['hdd_size'] = GeneralHelpers::convertToGB($value);
    }

    public function location(): BelongsTo
    {
        return $this->belongsTo(Location::class);
    }

    public function hddType(): BelongsTo
    {
        return $this->belongsTo(HddType::class);
    }

    /**
     * Calculate server HDD capacity field on database for future searches
     * This happens while sabing.
     */
    protected static function boot()
    {
        parent::boot();

        // Calculate server total capacity
        static::saving(function ($model) {
            $model->hdd_capacity = $model->hdd_quantity * $model->hdd_size;
        });
    }
}
