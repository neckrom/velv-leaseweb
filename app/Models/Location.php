<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Location extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use Attachable;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'code',
    ];

    protected $allowedSorts = [
        'name',
        'code',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $allowedFilters = [
        'name',
        'code',
    ];

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    /**
     * Finds a record based on Excel column value.
     *
     * @param string $value
     */
    public static function findByExcelValue(string $value)
    {
        return self::whereRaw('CONCAT(name, code) = ?', [trim($value)])->get()->first();
    }
}
