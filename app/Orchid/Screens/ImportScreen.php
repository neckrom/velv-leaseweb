<?php

namespace App\Orchid\Screens;

use App\Imports\ImportHddType;
use App\Imports\ImportLocation;
use App\Imports\ImportProduct;
use App\Models\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Orchid\Attachment\Models\Attachment;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class ImportScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'attachments' => Attachment::filters()->defaultSort('id')->paginate(),
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Upload Excel';
    }

    public function description(): ?string
    {
        return 'Choose an excel file to upload';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [];
    }

    /**
     * The screen's layout elements.
     *
     * @return Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Input::make('file')
                    ->type('file')
                    ->title('Choose a file ')
                    ->required()
                    ->horizontal(),

                Button::make('Upload')
                    ->method('import')
                    ->icon('folder')
                    ->horizontal()
            ])
        ];
    }

    public function import(Request $request)
    {
        $uploadedFile = $request->file('file');

        if ($uploadedFile) {
            Product::truncate();

            Excel::import(new ImportLocation(), $uploadedFile->store('files'));
            Excel::import(new ImportHddType(), $uploadedFile->store('files'));
            Excel::import(new ImportProduct(), $uploadedFile->store('files'));

            Alert::success('Data imported / updated');
        }
    }
}
