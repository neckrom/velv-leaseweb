<?php

namespace App\Orchid\Screens\Product;

use App\Models\Product;
use App\Orchid\Screens\Product\Filters\ProductRelatedFilter;
use App\Orchid\Screens\Product\Filters\ProductSortFilter;
use App\Orchid\Screens\Product\Layouts\PoductListLayout;
use Orchid\Screen\Screen;

class ProductListScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'products' => Product::filters()
                ->filtersApply([
                    ProductRelatedFilter::class,
                    ProductSortFilter::class
                ])
                ->defaultSort('id')->paginate(),
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Products';
    }

    public function description(): ?string
    {
        return 'Navigate and search thought the server list';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            PoductListLayout::class
        ];
    }
}
