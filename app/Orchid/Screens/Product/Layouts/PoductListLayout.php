<?php

namespace App\Orchid\Screens\Product\Layouts;

use App\Helpers\GeneralHelpers;
use App\Models\HddType;
use App\Models\Location;
use App\Models\Product;
use Orchid\Screen\Fields\RadioButtons;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PoductListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'products';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        $ram_types = Product::query()->select('ram_type')->orderBy('ram_type')->distinct()->get()->pluck('ram_type')->toArray();

        return [
            TD::make('id')
                ->sort(),

            TD::make('name')
                ->filter()
                ->sort(),

            TD::make('location.name', 'Location')
                ->filter(
                    Select::make()
                        ->fromModel(Location::class, 'name', 'name')
                        ->empty('(All)')
                )
                ->sort(),

            TD::make('ram_size', 'RAM Size')
                ->filter(
                    RadioButtons::make()
                        ->options(
                            array_combine(
                                Product::query()->select('ram_size')->orderBy('ram_size')->distinct()->get()->pluck('ram_size')->toArray(),
                                Product::query()->selectRaw("CONCAT(ram_size, ' GB') as ram_str, ram_size")->orderBy('ram_size')->distinct()->pluck('ram_str')->toArray()
                            )
                        )
                )
                ->sort()
                ->render(function ($model) {
                    return "{$model->ram_size} GB";
                }),

            TD::make('ram_type', 'RAM Type')
                ->filter(
                    RadioButtons::make()
                        ->options(
                            array_combine(
                                $ram_types,
                                $ram_types
                            )
                        )
                )
                ->sort(),

            TD::make('hdd_quantity', 'HDD Quantity')
                ->sort(),

            TD::make('hdd_size', 'HDD Size')
                ->sort()
                ->render(function ($model) {
                    return GeneralHelpers::formatBytes($model->hdd_size);
                }),

            TD::make('hdd_capacity', 'HDD Capacity')
                ->filter(
                    RadioButtons::make()
                        ->options(
                            array_flip(GeneralHelpers::$hdd_capacity_range)
                        )
                )
                ->sort()
                ->render(function ($model) {
                    return GeneralHelpers::formatBytes($model->hdd_capacity, 0);
                }),

            TD::make('hddType.name', 'HDD Type')
                ->filter(
                    Select::make()
                        ->fromModel(HddType::class, 'name', 'name')
                        ->empty('(All)')
                )
                ->sort(),

            TD::make('price')
                ->render(function ($model) {
                    return "€ {$model->price}";
                })
                ->sort(),
        ];
    }
}
