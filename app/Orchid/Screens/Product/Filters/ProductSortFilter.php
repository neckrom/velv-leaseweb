<?php

namespace App\Orchid\Screens\Product\Filters;

use Illuminate\Database\Eloquent\Builder;
use Orchid\Filters\Filter;
use Orchid\Screen\Field;

class ProductSortFilter extends Filter
{
    /**
     * The displayable name of the filter.
     *
     * @return string
     */
    public function name(): string
    {
        return '';
    }

    public function parameters(): ?array
    {
        return ['sort'];
    }

    /**
     * Apply to a given Eloquent query builder.
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        $param = $this->request->input('sort');

        $direction = str_starts_with($param, '-') ? 'desc' : 'asc';

        $param = $direction == 'desc' ? substr($param, 1) : $param;

        switch ($param) {
            case 'location.name':
                $this->builderFormatter($builder, 'location', 'name', $direction);
                break;
            case 'hddType.name':
                $this->builderFormatter($builder, 'hddType', 'name', $direction);
                break;
        }

        return $builder;
    }

    /**
     * Get the display fields.
     *
     * @return Field[]
     */
    public function display(): iterable
    {
        return [];
    }

    protected function builderFormatter(Builder &$builder, string $relation, string $search_column, string $direction)
    {
        $builder->whereHas($relation, function (Builder $query) use ($search_column, $direction) {
            $query->orderBy($search_column, $direction);
        });
    }
}
