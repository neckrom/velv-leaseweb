<?php

namespace App\Orchid\Screens\Product\Filters;

use Illuminate\Database\Eloquent\Builder;
use Orchid\Filters\Filter;
use Orchid\Screen\Field;

class ProductRelatedFilter extends Filter
{
    /**
     * The displayable name of the filter.
     *
     * @return string
     */
    public function name(): string
    {
        return '';
    }

    public function parameters(): ?array
    {
        return ['filter'];
    }

    /**
     * Apply to a given Eloquent query builder.
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        $request = $this->request->input('filter');

        if (isset($request['location.name'])) {
            $this->builderFormatter($builder, 'location', 'name', $request['location.name']);
        }

        if (isset($request['hddType.name'])) {
            $this->builderFormatter($builder, 'hddType', 'name', $request['hddType.name']);
        }

        if (isset($request['hdd_capacity'])) {
            foreach ($builder->getQuery()->wheres as $k => $this_where) {
                if ($this_where['column'] == 'hdd_capacity') {
                    $builder->getQuery()->wheres[$k]['operator'] = '>=';
                }
            }
        }

        return $builder;
    }

    /**
     * Get the display fields.
     *
     * @return Field[]
     */
    public function display(): iterable
    {
        return [];
    }

    protected function builderFormatter(Builder &$builder, string $relation, string $search_column, string $search_term)
    {
        $builder->whereHas($relation, function (Builder $query) use ($search_column, $search_term) {
            $query->where($search_column, '=', ($search_term ?? null));
        });
    }
}
