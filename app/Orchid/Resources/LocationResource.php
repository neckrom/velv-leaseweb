<?php

namespace App\Orchid\Resources;

use App\Models\Location;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;
use Orchid\Crud\Filters\DefaultSorted;
use Orchid\Crud\Filters\WithTrashed;
use Orchid\Crud\Resource;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Sight;
use Orchid\Screen\TD;

class LocationResource extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = Location::class;

    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields(): array
    {
        return [
            Input::make('name')
                ->title('Location Name'),
            Input::make('code')
                ->title('Location Code'),
        ];
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('name')
                ->filter()
                ->sort(),

            TD::make('code')
                ->filter()
                ->sort(),
        ];
    }

    /**
     * Get the sights displayed by the resource.
     *
     * @return Sight[]
     */
    public function legend(): array
    {
        return [
            Sight::make('id'),
            Sight::make('name'),
            Sight::make('code'),
            Sight::make('created_at', 'Created at')
                ->render(function ($model) {
                    return $model->created_at->toDateTimeString();
                }),
            Sight::make('updated_at', 'Updated at')
                ->render(function ($model) {
                    return $model->updated_at->toDateTimeString();
                }),
            Sight::make('deleted_at', 'Deleted at')
                ->render(function ($model) {
                    return $model->deleted_at ? $model->deleted_at->toDateTimeString() : null;
                })
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
            new DefaultSorted('name', 'asc'),
            new WithTrashed(),
        ];
    }

    public function rules(Model $model): array
    {
        return [
            'code' => [
                'required', Rule::unique(self::$model, 'code')->ignore($model),
            ],
        ];
    }

    public static function icon(): string
    {
        return 'location-pin';
    }

    public static function perPage(): int
    {
        return 30;
    }
}
