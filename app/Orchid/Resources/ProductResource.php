<?php

namespace App\Orchid\Resources;

use App\Models\HddType;
use App\Models\Location;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Orchid\Crud\Filters\DefaultSorted;
use Orchid\Crud\Filters\WithTrashed;
use Orchid\Crud\Resource;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\RadioButtons;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Sight;
use Orchid\Screen\TD;

class ProductResource extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = Product::class;

    public function with(): array
    {
        return ['location', 'hddType'];
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields(): array
    {
        return [
            Input::make('name')
                ->title('Name')
                ->required(),
            Select::make('location_id')
                ->fromModel(Location::class, 'name')
                ->title('Location')
                ->required(),
            Input::make('ram_size')
                ->title('RAM Size (GB)')
                ->type('number')
                ->required(),
            Input::make('hdd_quantity')
                ->title('HDD Quantity')
                ->type('number')
                ->required(),
            Input::make('hdd_size')
                ->title('HDD Size')
                ->type('number')
                ->required(),
            Select::make('hdd_type_id')
                ->fromModel(HddType::class, 'name')
                ->title('HDD Type')
                ->required(),

            Input::make('price')
                ->title('Price')
                ->required()
                ->mask([
                    'alias'          => 'currency',
                    'prefix'         => '€',
                    'groupSeperator' => ' ',
                    'digitsOptional' => true,
                ])
        ];
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('name')
                ->filter()
                ->sort(),

            TD::make('location.name', 'Location')
                ->filter(
                    /* Select::make()
                         ->fromModel(Location::class, 'name', 'name') */
                )
                ->sort(),

            TD::make('ram_size', 'RAM Size')
                ->filter(
                    RadioButtons::make('ram_size_filter')
                        ->options(
                            array_combine(
                                Product::query()->select('ram_size')->orderBy('ram_size')->distinct()->get()->pluck('ram_size')->toArray(),
                                Product::query()->selectRaw("CONCAT(ram_size, ' GB') as ram_str, ram_size")->orderBy('ram_size')->distinct()->pluck('ram_str')->toArray()
                            )
                        ),
                    ''
                )
                ->sort()
                ->render(function ($model) {
                    return "{$model->ram_size} GB";
                }),

            TD::make('ram_type', 'RAM Type')
                ->filter()
                ->sort(),

            TD::make('hdd_quantity', 'HDD Quantity')
                ->filter()
                ->sort(),

            TD::make('hdd_size', 'HDD Size')
                ->filter()
                ->sort(),

            TD::make('hdd_capacity', 'HDD Total Capacity')
                ->filter()
                ->sort(),

            TD::make('hddType.name', 'HDD Type')
                ->filter()
                ->sort(),

            TD::make('price')
                ->render(function ($model) {
                    return "€ {$model->price}";
                })
                ->sort(),
        ];
    }

    /**
     * Get the sights displayed by the resource.
     *
     * @return Sight[]
     */
    public function legend(): array
    {
        return [
            Sight::make('id'),
            Sight::make('name'),
            Sight::make('location.name', 'Location'),
            Sight::make('ram_size', 'RAM Size (GB)'),
            Sight::make('ram_type', 'RAM Type'),
            Sight::make('hdd_quantity', 'HDD Quanity'),
            Sight::make('hdd_size', 'HDD Size'),
            Sight::make('hddType.name', 'HDD Type'),
            Sight::make('price'),
            Sight::make('created_at', 'Created at')
                ->render(function ($model) {
                    return $model->created_at->toDateTimeString();
                }),
            Sight::make('updated_at', 'Updated at')
                ->render(function ($model) {
                    return $model->updated_at->toDateTimeString();
                }),
            Sight::make('deleted_at', 'Deleted at')
                ->render(function ($model) {
                    return $model->deleted_at ? $model->updated_at->toDateTimeString() : null;
                }),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
            new DefaultSorted('name', 'asc'),
            new WithTrashed(),
        ];
    }

    public static function icon(): string
    {
        return 'server';
    }
}
