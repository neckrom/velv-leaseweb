<?php

namespace App\Helpers;

class GeneralHelpers
{
    public static $hdd_capacity_range = [
        '>= 250GB' => 250,
        '>= 500GB' => 500,
        '>= 1TB'   => 1024,
        '>= 2TB'   => 2048,
        '>= 3TB'   => 3072,
        '>= 4TB'   => 4096,
        '>= 8TB'   => 8192,
        '>= 12TB'  => 12288,
        '>= 24TB'  => 24576,
        '>= 48TB'  => 49152,
        '>= 72TB'  => 73728
    ];

    protected static $sizeUnits = ['GB', 'TB', 'PB', 'EB'];

    /**
     * Converts HDD Size String (2TB) to GB value (2048).
     *
     * @param string $size
     *
     * @return float|int|null
     */
    public static function convertToGB(string $size): float|int|null
    {
        $size = is_numeric($size) ? "{$size}GB" : $size;

        $qty    = substr($size, 0, -2);
        $suffix = strtoupper(substr($size, -2));

        $exponent = array_flip(self::$sizeUnits)[$suffix];

        if ($exponent === null) {
            return null;
        }

        return $qty * (1024 ** $exponent);
    }

    public static function formatBytes(int $size_gb, int $precision = 2): string
    {
        // converto to byte
        $size_gb *= 1073741824;
        $base     = log($size_gb, 1024);
        $suffixes = ['', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB'];

        return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)];
    }
}
