<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('location_id')->constrained();
            $table->bigInteger('ram_size')->unsigned()->default(0);
            $table->string('ram_type');
            $table->integer('hdd_quantity')->unsigned()->default(1);
            $table->bigInteger('hdd_size')->unsigned()->default(0);
            $table->bigInteger('hdd_capacity')->unsigned()->default(0);
            $table->foreignId('hdd_type_id')->constrained();
            $table->integer('price');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
