<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        User::factory()->create([
            'name'        => 'admin',
            'email'       => 'admin@admin.com',
            'password'    => Hash::make('password'),
            'permissions' => [
                'platform.index'              => true,
                'platform.systems.roles'      => true,
                'platform.systems.users'      => true,
                'platform.systems.attachment' => true,
            ],
        ]);
    }
}
