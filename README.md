# About This Project

## Context

Hello, my name is Joel Silva and this is my project for Velv technical assignment.

## Application Stucture
- Laravel based application (it's what I'm most familiar with);
- Used Orchid package for RAD of backend;

## How to get up and running
1. Ensure you have PHP, Composer, Docker and docker-compose packages installed.
2. Clone this repo;
3. In the project folder, copy `.env.example` to `.env` (you may edit this configuration file);
4. Inside the cloned project run the command:
```
composer install --ignore-platform-reqs
docker-compose build
./vendor/bin/sail up -d
```
This will install composer dependencies, build and bring up all the docker containers.

Run `./vendor/bin/sail stop` to stop them.


5. Migrate the database:
```
./vendor/bin/sail artisan migrate:fresh
```
6. Seed the database with default user
```
./vendor/bin/sail artisan db:seed
```

## Admin Dashboard (Backend)
The application will be available on the localhost.

Pointing the browser to http://localhost/admin, will open backend.

Default credentials are:
- email: admin@admin.com
- password: password

## API for product (Server) browsing
Accessible via http://localhost/api/v1/products{/id}

## Tests
Tests are accessible running the command
```
./vendor/bin/sail artisan test
```
