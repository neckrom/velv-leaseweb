<?php

namespace Tests\Unit;

use App\Helpers\GeneralHelpers;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function test_that_2tb_is_2048gb()
    {
        $this->assertEquals(
            2048,
            GeneralHelpers::convertToGB('2TB'),
        );
    }

    public function test_that_24576gb_is_24tb()
    {
        $this->assertEquals(
            '24 TB',
            GeneralHelpers::formatBytes(24576)
        );
    }
}
