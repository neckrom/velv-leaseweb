<div class="bg-white rounded-top shadow-sm mb-3">

    <div class="row g-0">
        <div class="col col-lg-7 mt-6 p-4">

            <h2 class="mt-2 text-dark fw-light">
                Leaseweb Servers
            </h2>

            <p>
                Please explore the menu to navigate.
            </p>
        </div>
        <div class="d-none d-lg-block col align-self-center text-end text-muted p-4">
            <x-orchid-icon path="database" width="6em" height="100%"/>
        </div>
    </div>

    <div class="row bg-light m-0 p-md-4 p-3 border-top rounded-bottom">

        <div class="col-md-6 my-2">
            <h3 class="text-muted fw-light">
                <x-orchid-icon path="cloud-upload"/>

                <span class="ms-3 text-dark">Upload the Excel data</span>
            </h3>
            <p class="ms-md-5 ps-md-1">
                In order to populate initial data, you may upload the Excel file containing the Leaseweb Server data
                to be populated and accessible.
            </p>
        </div>

        <div class="col-md-6 my-2">
            <h3 class="text-muted fw-light">
                <x-orchid-icon path="rocket"/>

                <span class="ms-3 text-dark">Start Exploring</span>
            </h3>
            <p class="ms-md-5 ps-md-1">
                After uploading the Excel file, you may start exploring and filtering through the data.
            </p>
        </div>

    </div>

</div>

